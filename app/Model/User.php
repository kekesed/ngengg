<?php
Namespace Model;
Class User extends \DB\Cortex{
  protected
  $table = 'manusia',
  $db='DB',
   $fieldConf = [
     'username'=>['type'=>\DB\SQL\Schema::DT_VARCHAR512],
     'password'=>['type'=>\DB\SQL\Schema::DT_TEXT],
     'nama'=>['type'=>\DB\SQL\Schema::DT_TEXT],
     'email'=>['type'=>\DB\SQL\Schema::DT_TEXT],
     'blocked'=>['type'=>\DB\SQL\Schema::DT_BOOL, 'default'=>false],
     'user_type'=>['type'=>\DB\SQL\Schema::DT_TEXT]
    ];
   private function createSalt(){
     $factor = rand(5, 500);
     return md5(time() * $factor);
   }
   function set_password($value){
     $value = \Bcrypt::instance()->hash($value, $this->createSalt(), 14);
     return $value;
   }
   function verify($username, $password, $disallow_blocked=true){
     $user = clone $this;
     $user->load(array('username=? OR email=?', $username,$username));
     if($user->loaded()==0)
       throw new \Exception('User not found.', __LINE__);
     if($disallow_blocked && $user->blocked)
       throw new \Exception('User was blocked.', __LINE__);
     if(\Bcrypt::instance()->verify($password, $user->password)){
       return $user;
     } else {
       return false;
     }
   }
}
