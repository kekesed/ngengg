<?php
Namespace Services;
class Launchpad extends \Prefab{
  //register function
  function KickStart($x){
    $x->route('GET @authr: /auth', '\\Controller\\Auth->get_index');
    $x->route('POST @authr', '\\Controller\\Auth->post_index');
    $x->route('GET @auth: /auth/@met', '\\Controller\\Auth->get_@met');
    $x->route('POST @auth', '\\Controller\\Auth->post_@met');


    $x->route('GET @root: /', '\\Controller\\App->dash');
    //checking the database status
    if(\Cache::instance()->get('db_init') != true ){
      /// baru bikin:
      \Model\User::setup();
      \Cache::instance()->set('db_init', true);
    }
  }
}
