<?php
Namespace Controller;
class auth{
  function get_index($f3){
    //cek aja apa maunya dia .-.

  }

  function get_login($f3){
    \View\Auth::instance()->lempar('login');
  }
  function post_login($f3){
    if(!\Services\csrf::Validate($f3->POST['token'])) {
      \Flash::instance()->addMessage('Validation Error: Token expired.', 'error');
      \View\Auth::instance()->lempar('login');
      return false;
    }
    //mass check param
    $d = ['username','password'];
    $x = true;
    for($y = 0; $y < count($d); $y++)
      if($x && !$f3->exists('POST.'.$d[$y])){
        $x=false; break;
      }
    if(!$x){
      \Flash::instance()->addMessage('You need to fullfill those fields.', 'error');
      \View\Auth::instance()->lempar('login');
      return false;
    }

    $user = new \Model\User();
    try{
      $user = $user->verify($f3->POST['username'], $f3->POST['password']);
      //oke
    }catch(\Exception $e) {
      \Flash::instance()->addMessage($e->getMessage(), 'error');
      \View\Auth::instance()->lempar('login');
      return false;
    }


    //udah nggak apa apa
    $f3->set('COOKIE.user', $user->id);
    $f3->set('SESSION.user_type', $user->user_type);
    \F3::reroute('@root');
  }
  function get_register($f3){
    \View\Auth::instance()->lempar('register');
  }

  function post_register($f3){
    if(!\Services\csrf::Validate($f3->POST['token'])) {
      \Flash::instance()->addMessage('Validation Error: Token expired.', 'error');
      \View\Auth::instance()->lempar('register');
      return false;
    }

    //mass check param
    $d = ['name','email','username','password', 'password2'];
    $x = true;
    for($y = 0; $y < count($d); $y++)
      if($x && !$f3->exists('POST.'.$d[$y])){
        $x=false; break;
      }
    if(!$x){
      \Flash::instance()->addMessage('You need to fullfill those fields.', 'error');
      \View\Auth::instance()->lempar('register');
      return false;
    }

    //perform specific parameter checking
    $user = new \Model\User();
    if($user->count(array('username=?', $f3->POST['username'])) > 0){
      \Flash::instance()->addMessage('Username Exist', 'warning');
      \View\Auth::instance()->lempar('register');
      return false;
    }
    if($user->count(array('username=?', $f3->POST['username'])) > 0){
      \Flash::instance()->addMessage('Username Exist', 'warning');
      \View\Auth::instance()->lempar('register');
      return false;
    }

    if($f3->POST['password'] != $f3->POST['password2']){
      \Flash::instance()->addMessage('Your Password are insame.', 'warning');
      \View\Auth::instance()->lempar('register');
      return false;
    }

    //performing register.
    $user->reset();
    $user->copyfrom([
      'username'=>$f3->POST['username'],
      'password'=>$f3->POST['password'],
      'nama'=>$f3->POST['name'],
      'email'=>$f3->POST['email'],
      'user_type'=>'user'
    ]);
    $user->save();
    \Flash::instance()->addMessage('Register Success.', 'info');
    \F3::reroute('@auth(@met=login)');
  }
  function get_logout($f3){
    \Flash::instance()->addMessage('Bye, ' . $f3->get('system.user')->nama . '-senpai :\'(', 'success');
    $f3->clean('SESSION');
    $f3->set('COOKIE.user', null);
    \F3::reroute('@auth(@met=login)');
  }
}
