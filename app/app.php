<?php

//cheking database
if(!\F3::exists('DB')){
	//system disabled.
	\F3::route('GET /', '\\Controller\\App->newbie');
} else {
	\Services\Launchpad::instance()->Kickstart(\F3::instance());
\Middleware::instance()->before('GET|POST|PUT|DELETE *',function(\Base $f3, $param) {
	//cek apa dia login apa kagak, dan layak apa kagak. lel
	$access = \Access::instance();
	$access->policy('deny');
	$access->allow('/*', 'admin,user');
	$access->allow('GET|POST /Auth*');
	$access->allow('GET|POST /auth*');
	if($f3->exists("COOKIE.user") or $f3->exists("SESSION.user")) {
		$userz = new \Model\User();
		$userz->load(array('id=?', ($f3->exists("COOKIE.user")?$f3->COOKIE['user']:$f3->SESSION['user'])));
		$f3->set("system.user",$userz);
	}

	if(!$f3->exists('SESSION.user_type') && !$f3->exists('COOKIE.user'))
		$f3->set('SESSION.user_type', 'guest');
	else
		$f3->set('SESSION.user_type', $userz->user_type);

	$access->authorize($f3->get('SESSION.user_type'), function($route, $subject) {
		if(\F3::exists('DB'))
			\F3::reroute('@auth(@met=login)');
	});
});
}
