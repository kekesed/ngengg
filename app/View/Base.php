<?php
Namespace View;
Abstract Class Base extends \Prefab{
  function _lempar($layout = 'layout.php', $formable=false){
    if($formable)
      $this->form_init();
    echo \Template::instance()->render($layout);
  }


  function form_init() {
    //generate CSRF dan inject ke @form :* :*
    \F3::set('form.csrf', \Services\csrf::Get());
    //done. tbh, it's so ez.
  }
}
